import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { UiModule } from './ui/ui.module';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found/page-not-found.component';
import { CarsComponent } from './views/cars/cars.component';
import { FormAddCarComponent } from './views/form-add-car/form-add-car.component';


@NgModule({
  declarations: [AppComponent, PageNotFoundComponent, CarsComponent, FormAddCarComponent ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    UiModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
