import { Injectable } from '@angular/core';
import { CarsI } from '../shared/interfaces/cars-i';

@Injectable({
  providedIn: 'root',
})
export class CarsService {
  private cars: CarsI[] = [];

  constructor() {}

  createCar(car: CarsI) {
    const newCar = { ...car, id: Date.now(), delete: false }
    this.cars = [...this.cars, newCar];
    console.log(this.cars);
  }

  getCars(){
    return this.cars;
  }

  deleteCar(car:CarsI){
    this.cars = this.cars.filter(c => c.id !== car.id)
  }

}
