import { StateCars, ColorCars } from '../enums/state-cars.enum';
import { CarsI } from '../interfaces/cars-i';

export class Cars implements CarsI {
  nom!: string;
  marque!: string;
  immatriculation!: string;
  date!: string;
  color = ColorCars.DEFAULT;
  state = StateCars.DEFAULT;
  id?: number;
  delete?: boolean = false

  constructor(obj?: Partial<Cars>){
    if(obj) {
      Object.assign(this, obj);
    }
  }
}
