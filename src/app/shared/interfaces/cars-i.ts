import { StateCars, ColorCars } from '../enums/state-cars.enum';

export interface CarsI {
  nom: string,
  marque: string,
  immatriculation: string,
  date: string,
  color: ColorCars,
  state: StateCars,
  id?: number,
  delete?: boolean
}

