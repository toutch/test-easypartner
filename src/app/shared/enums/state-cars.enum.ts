export enum StateCars {
  DEFAULT = " --- ",
  BON = "Bon",
  MOYEN = "Moyen",
  MAUVAIS = "Mauvais"
}

export enum ColorCars {
  DEFAULT = " --- ",
  BLANCHE = "Blanche",
  NOIRE = "Noire",
  ROUGE = "Rouge",
  GRISE = "Grise",
  VERTE = "Verte"
}

