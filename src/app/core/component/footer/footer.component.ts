import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public idUser!: number;

  constructor() { }

  ngOnInit(): void {
    localStorage.userId = Date.now();
    this.idUser = localStorage.userId;
  }

}