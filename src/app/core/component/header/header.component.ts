import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public title: string = '';
  public oldest: string = '';
  public nbCars: number = 0;
  public WeatherData: any;
  public parisWeather: string = '';
  public weatherIcon: string = '';

  private urlApi: string = "http://api.weatherapi.com/v1/current.json?key=5b1c996af05b495ba03155037212006&q=paris&aqi=no";

  constructor() {}

  ngOnInit(): void {
    this.title = 'Une bien belle collection';
    this.oldest = 'XX-XXX-XX - Pontiac - Bleu - 21 ans';

    this.getWeatherData();
  }

  getWeatherData(){
    fetch(this.urlApi)
    .then(response=>response.json())
    .then(data=>{this.setWeatherData(data);})

  // let data = JSON.parse('{"location":{"name":"Paris","region":"Ile-de-France","country":"France","lat":48.87,"lon":2.33,"tz_id":"Europe/Paris","localtime_epoch":1624212205,"localtime":"2021-06-20 20:03"},"current":{"last_updated_epoch":1624211100,"last_updated":"2021-06-20 19:45","temp_c":19.0,"temp_f":66.2,"is_day":1,"condition":{"text":"Light rain shower","icon":"//cdn.weatherapi.com/weather/64x64/day/353.png","code":1240},"wind_mph":11.9,"wind_kph":19.1,"wind_degree":90,"wind_dir":"E","pressure_mb":1005.0,"pressure_in":30.2,"precip_mm":0.0,"precip_in":0.0,"humidity":94,"cloud":75,"feelslike_c":19.0,"feelslike_f":66.2,"vis_km":8.0,"vis_miles":4.0,"uv":6.0,"gust_mph":8.9,"gust_kph":14.4}}')
  // this.setWeatherData(data);
  }

  setWeatherData(data:any){
    this.WeatherData = data;
    this.parisWeather = this.WeatherData.current.condition.text;
    this.weatherIcon = this.WeatherData.current.condition.icon;
  }
}
