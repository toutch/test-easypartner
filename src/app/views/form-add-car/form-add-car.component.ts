import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { CarsService } from 'src/app/services/cars.service';
import { ColorCars, StateCars } from 'src/app/shared/enums/state-cars.enum';
import { CarsI } from 'src/app/shared/interfaces/cars-i';
import { Cars } from 'src/app/shared/models/cars.model';

@Component({
  selector: 'app-form-add-car',
  templateUrl: './form-add-car.component.html',
  styleUrls: ['./form-add-car.component.scss']
})
export class FormAddCarComponent implements OnInit {

  @Input() car = new Cars();
  @Output() clicked: EventEmitter<Cars> = new EventEmitter();

  public states = Object.values(StateCars);
  public colors = Object.values(ColorCars);
  
  public form!: FormGroup;

  constructor(private cs: CarsService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createFormulaire();
  }

  public onSubmit(): void {
    this.clicked.emit(this.form.value)
  }

  public createFormulaire(): void {
    this.form = this.formBuilder.group({
      nom: [
        this.car.nom,
        Validators.required
      ],
      marque: [
        this.car.marque,
        Validators.required
      ],
      immatriculation: [
        this.car.immatriculation,
        Validators.compose([Validators.required, Validators.minLength(7)])
      ],
      date: [this.car.date],
      couleur: [this.car.color],
      state: [this.car.state]
    })
  }


}
