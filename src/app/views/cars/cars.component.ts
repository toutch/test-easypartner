import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CarsService } from 'src/app/services/cars.service';
import { StateCars, ColorCars } from 'src/app/shared/enums/state-cars.enum';
import { CarsI } from 'src/app/shared/interfaces/cars-i';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss'],
})
export class CarsComponent implements OnInit {
  public states = Object.values(StateCars);
  public colors = Object.values(ColorCars);
  
  fc!: FormGroup;
  nomCtrl!: FormControl;

  allCars: CarsI[] = [
    {
      nom: '2cv',
      marque: 'Citroën ',
      immatriculation: '01AAA01',
      date: '1970',
      color: ColorCars.GRISE,
      state: StateCars.MOYEN,
      id: 1624304978069,
      delete: false,
    },
  ];

  constructor(private cs: CarsService, fb: FormBuilder) {
    this.nomCtrl = fb.control('', Validators.required);
    this.fc = fb.group({
      nomCtrl: this.nomCtrl,
    });
  }

  ngOnInit(): void {}

  addCar(form: any) {
    this.cs.createCar(form.value);
    form.reset();
    this.getCar();
  }

  getCar() {
    const allCarsTempo = this.cs.getCars();
    this.allCars = allCarsTempo.filter((ac) => !ac.delete);
  }

  delete(car: CarsI) {
    this.cs.deleteCar(car);
    this.getCar();
  }

}
