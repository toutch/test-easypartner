import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarsComponent } from './views/cars/cars.component';
import { PageHomeComponent } from './views/home/page-home/page-home.component';
import { PageNotFoundComponent } from './views/page-not-found/page-not-found/page-not-found.component';


const routes: Routes = [
  { path: 'home', component: PageHomeComponent },
  { path: 'cars', component: CarsComponent },
  { path: '', redirectTo: "home", pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
